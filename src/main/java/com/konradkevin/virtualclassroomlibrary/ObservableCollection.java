package com.konradkevin.virtualclassroomlibrary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


// Proxy , Observer, Iterator
public class ObservableCollection<T> implements IObservable, IIterable {
    // Items
    private List<T> items;
    /// The observers
    private List<IObserver> observers;

    public ObservableCollection() {
        this.items = new ArrayList<T>();
        this.observers = new ArrayList<IObserver>();
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "ObservableCollection{" +
                "items=" + items +
                '}';
    }

    @Override
    public void addObserver(IObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        this.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer :
                this.observers) {
            observer.update();
        }
    }

    @Override
    public void setChanged() {
        this.notifyObservers();
    }


    // Iterator

    @Override
    public IIterator getIterator() {
        return new ObservableCollectionIterator();
    }


    private class ObservableCollectionIterator implements IIterator {

        private int index = 0;

        @Override
        public boolean hasNext() {
            if(index < items.size()){
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if(this.hasNext()){
                return items.get(index++);
            }
            return null;
        }
    }

    // End Iterator

    // proxy methods start
    public int size() {
        return this.items.size();
    }

    public boolean isEmpty() {
        return this.items.isEmpty();
    }

    public boolean contains(Object o) {
        return this.items.contains(o);
    }

    public Iterator<T> iterator() {
        return this.items.iterator();
    }

    public Object[] toArray() {
        return this.items.toArray();
    }

    public <T1> T1[] toArray(T1[] t1s) {
        return this.items.toArray(t1s);
    }

    public boolean add(T t) {
        this.setChanged();
        return this.items.add(t);
    }

    public boolean remove(Object o) {
        this.setChanged();
        return this.items.remove(o);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.items.containsAll(collection);
    }

    public boolean addAll(Collection<? extends T> collection) {
        this.setChanged();
        return this.items.addAll(collection);
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        this.setChanged();
        return this.items.addAll(i, collection);
    }

    public boolean removeAll(Collection<?> collection) {
        this.setChanged();
        return this.items.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        this.setChanged();
        return this.items.retainAll(collection);
    }

    public void clear() {
        this.setChanged();
        this.items.clear();
    }

    public T get(int i) {
        return this.items.get(i);
    }

    public T set(int i, T t) {
        this.setChanged();
        return this.items.set(i, t);
    }

    public void add(int i, T t) {
        this.setChanged();
        this.items.add(i, t);
    }

    public T remove(int i) {
        this.setChanged();
        return this.items.remove(i);
    }

    public int indexOf(Object o) {
        return this.items.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        return this.items.lastIndexOf(o);
    }

    public ListIterator<T> listIterator() {
        return this.items.listIterator();
    }

    public ListIterator<T> listIterator(int i) {
        return this.items.listIterator();
    }

    public List<T> subList(int i, int i1) {
        return this.items.subList(i, i1);
    }
    // proxy methods end
}
