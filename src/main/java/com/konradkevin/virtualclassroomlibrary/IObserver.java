package com.konradkevin.virtualclassroomlibrary;

public interface IObserver {

    /// Updates this instance.
    void update();
}
