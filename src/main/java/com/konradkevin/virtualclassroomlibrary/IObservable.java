package com.konradkevin.virtualclassroomlibrary;

public interface IObservable {

    /// Adds the observer.
    void addObserver(IObserver observer);

    /// Removes the observer.
    void removeObserver(IObserver observer);

    /// Notifies the observers.
    void notifyObservers();

    /// Sets the changed.
    void setChanged();
}
