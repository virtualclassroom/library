package com.konradkevin.virtualclassroomlibrary;

public interface IIterator {
    public boolean hasNext();
    public Object next();
}
