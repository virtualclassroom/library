package com.konradkevin.virtualclassroomlibrary.models;

public class Attachment {
    private String name;
    private String url;
    private int downloads;
    private boolean downloaded;

    public Attachment() { }

    public Attachment(String name, String url, boolean downloaded) {
        this.name = name;
        this.url = url;
        this.downloaded = downloaded;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public static Attachment.Builder getBuilder() {
        return new Attachment.Builder();
    }

    public static class Builder {

        private Attachment instance;

        public Builder() {
            this.instance = new Attachment();
        }

        public Attachment.Builder name(String value) {
            this.instance.name = value;
            return this;
        }

        public Attachment.Builder url(String value) {
            this.instance.url = value;
            return this;
        }

        public Attachment.Builder downloads(int value) {
            this.instance.downloads = value;
            return this;
        }

        public Attachment.Builder downloaded(boolean value) {
            this.instance.downloaded = value;
            return this;
        }
    }
}
